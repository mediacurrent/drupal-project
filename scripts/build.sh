#!/bin/bash

SCRIPT_DIR=$(cd $(dirname "$0") && pwd -P)
BASE_DIR=$(cd ${SCRIPT_DIR}/.. && pwd -P)

cd ${BASE_DIR}

CMD=$@
# Default to run "site:build" if no commands provided.
if [ -z "${CMD}" ]; then
  CMD="site:build -Dexisting_config --verbose"
  if [ -z "$(ls -A ${BASE_DIR}/config/sync/*.yml 2>/dev/null)" ]; then
    CMD="site:build --verbose"
  fi
fi

${BASE_DIR}/scripts/hobson ${CMD}
